# Designing Data-Intensive Application Ch 2

* _polyglot persistence_

---

## (Historical) hierarchical and network model

* CODASYL
* _access path_
  * Low-level access reference

---

## SQL vs NoSQL

* large datasets, high write throughput
* OO-RDB impedance problem
* locality
  * but also consider: interleaving (Spanner), multi-table index cluster tables (Oracle), column family (BigTable)
* _shredding_ : splitting a document-like structure into multiple tables
* many-to-one, many-to-many, join, normalization
  * data has a tendency of becoming more interconnected as features are added to applications.
* schema-at-read
  * flexibility?
  * `ALTER TABLE` downtime (MySQL)

---

## Convergence

* XML and JSON column in SQL database
  * Indexable, Queryable

---

## SQL

* Declarative
* Can be run in parallel
* Easy to reason about, and optimize

---

## MongoDB-style MapReduce

* Low level. SQL can be implemented as a pipeline of MapReduce (e.g. Apache Hive)
* Being able to use JavaScript
* MongoDB now includes more declarative _aggregation pipeline_

---

## Graph

### Property graphs

* vertex: ID, outgoing edges, incoming edges, properties key-value pairs
* edge: ID, tail vertex, head vertex, label, properties key-value pairs
* Neo4j, Cypher query language
* Can be done with recursive CTE
---

### Triple-stores

* (subject, predicate, object)
* object can be reference to another subject, or primitives
* historical note on semantic web and RDF
* SPARQL, Datalog (Datomic, Cascalog)
* 6th-normal form, E-A-V model

---

# Extra discussions

## Cloud Firestore

* https://firebase.google.com/docs/firestore/data-model
* Document, Collection, Subcollection
* ID generation

---

## GraphQL

* http://graphql.org/
* More like SQL then NoSQL query language?
* Notably, no infinite recursion

---

## AWS Neptune

* https://aws.amazon.com/neptune/
* Gremlin, SPARQL, RDF
