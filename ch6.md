# Designing Data-Intensive Application Ch 6: Partitioning

Partitioning == Sharding == Breaking up the data into partitions

* Required for very large datasets or very high query throughput
* Mainly used for scalability
* Normally each piece of data belongs to exactly one partition
* Each partition may be replicated but normally partitioning scheme is orthogonal to replication scheme.

## Partitioning K,V Data

* Want to distribute data and query load. (Avoid hot spots.)
* Simple solution is random distribution. However, this does not help when reading a particular item.

### Partition by Key Range

* Assign a continuous range of keys to each partition.
* In order to distribute the data evenly, partition boundaried need to adopt to the data.
* Boundary may be chosen
  * manually by an admin
  * choose automatically (Bigtable, HBase ...)
* Keep keys in sorted order within partition for easy range scans.
* Downside is potential hot spots.
* If you partition writes cleverly, your range query may become difficult. (Sensor example.)

### Partition by Hash of Key

* Goal is uniform distribution
* Boundary can be uniform or use *Consistent Hashing*. (This actually doesn't work so well for databases. This is explaines later.)
* Range query is hard again
* Cassandra uses a compromise.
  * Can declare a *compound primary key* which consists of multiple columns.
  * Only first part of key is hashed to determine the partition.
  * Other columns are used as a concatenated index for sorting the data in Cassandra's SSTables.
  * Good for one-to-many data relationships; can range query efficiently for a specific first part key.

### Skewed Workloads and Relieving Hot Spots

* Skewing can still happen - e.x. celebrity accounts.
* No elegant data store level solution.
* Can solve write by pre/postpending a random number to the key.
* This does require
  * More work when reading
  * Additional book keeping
* Thus makes sense to only append this random number to a small number of hot keys.

## Partitioning and Secondary Indexes

* Secondary indexes are bread and butter of relational DBs and are common in document based DBs as well.
* Problem w/ secondary indexes is that they don't map neatly to partitions.
* The two main approaches are `document-based` and `term-based` partitioning.

### Partitioning Secondary Indexes by Document

* Each partition persists secondary indexes in the form of `Secondary Index -> Primary Index`
* Searching requires querying all partitions and then combining the result. Also called *scatter and gather*. This can be pretty expensive.
* Even if querying each partition is run in parallel, this is prone to tail latency amplification.
* DB vendors suggest that users structure the partitioning scheme so that the secondary index queries can be served from a single partition. This is hard, especially if querying by multiple secondary keys.

### Partitioning Secondary Indexes by Term

* Use a *global index*
* This global index needs to be partitioned (or otherwise it would defeat the purpose of partitioning)
* We can again partition the indexes by term or the hash of the term. Trade off is the same as before.
* Reads are more efficient than the previous method.
* Writes are less efficient as index update for a single write may be across multiple partitions.

## Re-balancing Partitions

Over time, things change in a DB:

* Query throughput increases -> want to add more CPU
* Dataset size increases -> want more RAM/Disk
* Machine failure

These require data to be moved from a node to another.
Re-balancing is usally expected to meet some minimum requirements:

* Load (storage, query throughput) should be shared fairly after re-balancing
* DB should continue accepting read/writes while rebalancing
* Rebalancing should only mobe necessary data to make the process fast and minimize network/disk I/O load

### Stragetiges for Rebalancing

#### ~hash mod N~ (Don't do this)

N is # of nodes. Adding a node is very expensive in this case.

#### Fixed number of partitions

* Create more partitions than there are nodes and assign several partitions to each node.
* Re-balancing is simply moving partitions around to a new node.
* Change of assignment is not immediate - takes time for the data to transfer.
* Can account for mismatched hardware in the cluster by assigning different number of partitions per node
* Used in Riak, Elastisearch, Couchebase, Voldemort etc
* Initial max # of partitions is final max. (Well, there are ways to split partitions but this adds complexity)
* If partitions are too large, rebalancing and node failure recovery is expensive
* If partitions are too small, they incur too much overhead.

#### Dynamic Partitioning

* In the case of key range partitioning, fixed # of partitions w/ fixed boundaries can be inconvenient as dataset grows.
* HBase, RethinkDB creates partitions dynamically.
* Advantage - # of partition adapts to the total data volume.
* Caveat - an empty DB starts w/ a single partition. All writes go to a single node.
* HBase and MongoDB allow an initial set of partitions to be configured.

#### Partitioning Proportionally to nodes

* Have a fixed # of partitions per node
* When a new node joins the cluster, it randomly chooses a fixed # of existing partitions to split.
* Random can create an unfair rebalancing. Cassandra 3.0 introduces an alternative algorithm that avoids unfair splits.
* Random partition boundary selection requires hash-based partitioning.
* Used by Cassandra and Ketama

### Operations: Automatic or Manual Rebalancing

* Not binary; there is a gradient btwn full auto and full manual.
* Full auto is convenient but unpredictable.
* Having a human in the loop can be a good idea.

## Request Routing

How does the DB know which node to route a request to? Couple of options:

* Round-robin load balancer + forwarding
* Routing tier eacts as a partition aware load balancer
* Clients are aware of partitioning and assignment

Key problem is: how does the component making the routing decision learn about changes in the assignment of partitions to nodes?
This requires consensus but is not easy to implement correctly. (More in Chapter 9)

* Many rely on a separate coordination service such as ZooKeeper to keep track of cluster metadata. (Espresso, HBase, SolrCloud, Kafka etc)
* *Gossip Protocol* works as well. (Cassandra, Raik)

## Parallel Query Execution

Discussed in Chapter 10.
