# Designing Data-Intensive Application Ch 7 : Transaction

Why transaction?
* simplify the programming model through the safety guarantees
* often-made analogy: garbage collection in memory management

## ACID

* **Atomicity**: failed transaction rolls back dirty writes. _abortability_
  * Not to be confused with: atomic increment / decrement in multithreading. atomism.
* **Consistency**: invariants (think `UNIQUE`)
  * Not to be confused with consistency in distributed systems
* **Isolation**: concurrently executing transactions are isolated from each other: they cannot step on each others’ toes
  * Different levels of isolation:

| Book               | Pgsql           | Oracle          | MySQL            |
|--------------------|-----------------|-----------------|------------------|
| Read Committed     | Read Committed^ | Read Committed^ | Read Committed   |
| Snapshot Isolation | Repeatable Read | _Serializable_  | Repeatable Read^ |
| Serializable       | Serializable    | ???             | Serializable     |

^: default setting

* **Durability**: once a transaction has committed successfully, any data it has written will not be forgotten, even if there is a hardware fault or the database crashes
  * Usually involves write-ahead log

## Single-object writes

* Provides Atomicity and Isolation with compare-and-set and atomic operations.
* Sometimes called "lightweight transactions" but misleading.

## Handling errors and aborts

Why retry isn't perfect:
* Timeout
* Overload
* Permanent error: not worth retrying
* Side-effects
* Client fails while retrying - this data is lost

## Race conditions and Isolation levels

| Race condition | What it is                                                                                                                    | Isolation level                                                                     |
|----------------|-------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------|
| Dirty reads    | Reads another client’s writes before committed                                                                                | Read Committed                                                                      |
| Dirty writes   | Overwrites data that another client has written, but not yet committed                                                        | All transaction implementations                                                     |
| Read skew      | Sees different parts of the database at different points in time                                                              | Snapshot Isolation                                                                  |
| Lost updates   | Two clients concurrently perform a read-modify-write cycle. One overwrites the others write without incorporating its changes | Some Snapshot Isolation                                                             |
| Write skew     | Reads something. Makes a decision. When the write is made, the premise is no longer true                                      | Serialization                                                                       |
| Phantom reads  | Reads objects that match some search condition. Another client makes a write that affects the results of that search          | Snapshot Isolation (read-only transactions), Some requires special treatment        |

---

# Read Committed

* No dirty reads. 
  * Why? Partially updated state is confusing. Values might be rolled back
* No dirty writes
  * Why? conflicting writes can be mixed up (The sale to Bob, the invoice to Alice)
  * Note it doesn't prevent lost updates

## Implementation

* Writes: row-level write locks
* Reads: 
  * Read locks: harms response time for read-only transactions
  * Database remembers committed values

---

# Snapshot isolation

* Each transaction reads from a consistent snapshot of the database
* When do we need snapshot isolation:
  * Backups: restoring can make the inconsistencies permanent
  * Analytic queries and integrity checks. Long-running queries

## Implementation

* Writes: write locks
* Reads: no lock
  * _readers never block writers, and writers never block readers_

## Multi-Version Concurrency Control (MVCC)

* Database keeps several different committed versions of an object
* When starting transaction - given unique, always-incrementing transaction ID
* Each row has _created by_ and _deleted by_ fields. Updates are translated into delete and create
  * Deleted objects get garbage collected later
* Visibility rules:
  1. Ignored if from transaction in progress at the start of transaction
  1. Ignored if from aborted transaction
  1. Ignored if writes made by txid > current txid
* By never updating values in place, the database can provide a consistent snapshot while incurring only a small overhead
* Indexes: one option is to have all versions indexed and filter out when reading

## Append-only / Copy-on-write B-trees

* CouchDB, Datomic, LMDB
  * Particular root is a consistent snapshot of the database at the point in time when it was created
  * Requires compaction and GC

## Preventing lost updates

* Two concurrently running read-modify-write cycle
* Scenarios
  * Incrementing a counter or updating an account balance
  * Making a local change to a complex (e.g. JSON) value
  * Concurrent edits

### Atomic write operations 
  * Most RDBMS supports
  * NoSqls also provide atomic operations

```sql
UPDATE counters SET value = value + 1 WHERE key = 'foo';
```
  
### Explicit locking

```sql
BEGIN TRANSACTION;

SELECT * FROM figures
WHERE name = 'robot' AND game_id = 222
FOR UPDATE;
-- Check whether move is valid, then update the position
-- of the piece that was returned by the previous SELECT.
UPDATE figures SET position = 'c4' WHERE id = 1234;

COMMIT;
```

`FOR UPDATE` locks on all rows returned by the query

### Automatically detecting lost updates

* Transactions with lost updates are aborted and tried again: PgSQL _repeatable read_, Oracle's _serializable_, MSSQL's _snapshot_ isolation level.
  * Not MySQL InnoDB's _repeatable read_

### Compare-and-set

* update only happen if the value has not changed since you last read it
* if the database allows the `WHERE` clause to read from an old snapshot, this statement may not prevent lost updates

### Conflict resolution and replication

* a common approach is to allow concurrent writes to create several conflicting versions of a value (also known as _siblings_), and to use application code or special data structures to resolve and merge these versions after the fact.
* commutative atomic operations, CRDT (Riak 2.0)
* last-write wins: prone to lost updates

## Preventing write skew and phantoms

* write skew: two transactions read the same objects, and then update some of those objects
  * if updating same object: dirty write or lost update
* No automatic detection (unless true serializable isolation)
* Databases support some built-in invariant constraints (e.g. `UNIQUE`)
* Examples:
  * Meeting room booking system. Avoid double-booking
  * Multiplayer game
  * Claiming a username
  * Preventing double-spending
* phantom: a write in one transaction changes the result of a search query in another transaction
* Explicit locking can prevent some phantoms (but not checking for _absence_)

```sql
BEGIN TRANSACTION;

SELECT * FROM doctors
WHERE on_call = true
AND shift_id = 1234 FOR UPDATE;

UPDATE doctors
SET on_call = false
WHERE name = 'Alice'
AND shift_id = 1234;

COMMIT;
```

* materializing conflicts: artificially introduce a lock object into the database
  * meeting room booking case: a table of time slots and rooms. Each row in this table corresponds to a particular room for a particular time period. You create rows for all possible combinations of rooms and time periods ahead of time.
  * can be hard and error-prone
  * ugly to let a concurrency control mechanism leak into the application data model
  * last resort

---

# Serializability

* Guarantees that even though transactions may execute in parallel, the end result is the same as if they had executed one at a time, serially, without any concurrency.

## Actual serial execution

* RAM is cheap
* OLTP transactions are usually short, only make a small number of reads and writes
* VoltDB/H-Store, Redis and Datomic
* Stored procedures
  * Interactive queries: bottleneck on network
* Pain points
  * each vendor has their own language: modern stored procs use general-purpose lanugage
  * difficult to manage (version control and deploy). 
  * badly written code can kill the performance.
* Partitioning
  * Scale to multiple CPU cores and multiple nodes
  * cross-partition transactions are vastly slower: VoltDB reports a throughput of about 1,000 cross-partition writes per second

## Two-phase locking

* Sometimes called strong strict two-phase locking (SS2PL)
* Locks on read-write conflict

### Implementation

* Acquire (or upgrade) shared lock (for reads) or exclusive lock (for writes)
* Keep locks until the end of transaction, then release
* Detect deadlocks and abort
* Performance significantly worse
  * Due to reduced concurrency
  * Unstable latencies. Very slow at high percentiles
  * Deadlock more frequent
* Predicate locks: belongs to all objects that match some search condition
  * applies even to objects which do not yet exist in the database, but might be added in future
  * If two-phase locking includes predicate locks, the database prevents all forms of write skew and other race conditions, and so its isolation becomes serializable.

```sql
SELECT * FROM bookings
  WHERE room_id = 123 AND
    end_time > '2015-01-01 12:00' AND
    start_time < '2015-01-01 13:00';
```

* Index-range locks (a.k.a. next-key locking): approximation of predicate locking
  * predicate locks do not perform well
  * simplify a predicate by making it match a greater set of objects
  * locks attached to a range of values in an index. if another transaction wants to insert, update or delete, it will have to update the same part of the index. It will encounter the shared lock, and will be forced to wait until the lock is released.
  * fall back to locking the entire table

## Serializable Snapshot Isolation

* PgSQL since 9.1, FoundationDB
* optimistic concurrency control
  * if there is enough spare capacity, and if contention between transactions is not too high, optimistic concurrency control techniques tend to perform better than pessimistic ones
* On top of snapshot isolation, SSI adds an algorithm for detecting serialization conflicts among writes, and determining which transactions to abort.
* Assume any change in the query result (the premise) means that writes in that transaction may be invalid

Two cases:
1. Detecting stale MVCC reads (uncommitted write occurred before the read)
1. Detecting writes that affect prior reads (the write occurs after the read)

### Detecting stale MVCC reads

* Track when a transaction ignores another transactions' writes due to MVCC visibility rules
* When the transaction wants to commit, the database checks whether any of the ignored writes have now been committed. If yes, the transaction must be aborted.


### Detecting writes that affect prior reads

* Use index range lock
* When a transaction writes to the database, it must look in the indexes for any other transactions that have recently read the affected data. notifies the transactions that the data they read may no longer be up-to-date.
