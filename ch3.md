# Designing Data-Intensive Application Ch 3

## Database's two basic operations

1.  How to store the data
1.  How to find the data

## Why study storage engine internals

Choosing a right storage engine based on your needs (workload).

### Workload

*   Transactions (OLTP)
*   Analytics (OLAP)

### Engine type

1.  Log-structured storage engine
    *   Bigtable
    *   Riak
    *   Cassandra
    *   Lucene
    *   Rocks DB
    *   Level DB
    *   HBase
    *   Dremel (**Column-oriented storage**)

1.  Page-oriented storage engine (B-trees)
    *   MySQL
    *   PostresSQL
    *   HyperDex
    *   Oracle
    *   MongoDB

## Data structures in databases

### Logs

*   **log** is an append-only sequence of records
*   Note: different from **application logs** that describes what's happening in human-readable text.
*   Cost: Efficient writes in O(1); but slow lookup of O(n).
*   Append-only vs. in-place logs
    *   sequential writes are faster than random writes.
    *   concurrency and crash recovery is simpler.
    *   merging prevents segment fragmentation over time.
*   Example:
    *   [Google RecordIO](https://developers.google.com/optimization/reference/base/recordio/)
        *   Efficient file read/write for many small objects
        *   Compression. Protobuffer supported.

### Hash Indexes

*   **index** is an additional structure (metadata) for the primary data.
    *   Not affecting the primary data.
    *   Trade-offs: Speeds up reads but slows down writes.
        *   Thus, developers or admin should know which data needs to be indexed

*   Key-value Store = Key Indexes + Key-value data.
*   Hash indexes implemented as a hash table are commonly used.

*   In-memory hash map for indexes + key-value data on disk
    *   M[key] => file offsets.
    *   Good when the number of keys is not too large and the value of each key is updated frequently.
    *   Bitcask used in Riak
        *   Keeps in-memory hash map for indexes.

*   Compaction to the rescue of space issues in append-only logs
    *   Compaction: throwing away duplicate keys and keeping the most recent update for each key.
    *   Segments: breaking the log into multiple segments (files).
    *   Merging: merging multiple segments as performing compactions. It outputs a new segment file.
    *   Snapshotting/freezing: compaction/merging happens in a background thread while serving read/write requests.
    *   Lookup the value for a key: Check from the most recent segment's hash map.

*   Limitations (still)
    *   hash table must fit in memory.
    *   range queries are not efficient.

*   Implementation details
    *   File format: binary format. Encoding and compression. (length of data)(blob)
    *   Deleting records: append a special deletion record (tombstone) to the data file. Merging cleans up deleted keys.
    *   Crash recovery: in-memory hashmap generation requires the entire segment file scan. Speed up by storing a snapshot of hashmap on disk.
    *   Partially written records: checksums
    *   Concurrency control: one writer thread to force sequenial order; many reader threads.
        
### SSTables (and LSM-Trees)

*   Sorted String Table: the sequence of key-value pairs is sorted by keys.
    *   Merging segments is efficient: k-array merge takes O(n lg k)
    *   Sparse index in memory: jump to the closest key in the index, and scan from there.
    *   Block compression to save space and I/O bandwidth.

*   Maintaining SSTables
    *   memtable: in-memory tree
    *   When a write comes in, add it to the memtable
    *   When memtable hits some threshold, write a SSTable on disk.
    *   Read requests: memtable -> most recent SSTable -> older SSTables
    *   Crash recovery: logging on disk to recover the memtable.
    
*   Log-Structured Merge-Tree (or LSM-Tree) vs. SSTables + Memtable

*   Performance optimizations
    * Bloom Filters: Probabilistic set. Contains(item) => (maybe|no)
    * Merge compation strategy

### B-Trees

*   Standard index implementation in almost all relational databases.
*   Good for value lookups and range queries.
*   B-trees are balanced trees:
    *   O(log n) for read/write.
    *   The lookup always starts from the root page.
    *   One page to refer to another (pointer).
    *   B-trees have fixed-size blocks (called pages).
    *   Read or write one page at a time.

*   Implementation details
    *   Crash recovery: write-aheadlog (WAL)
    *   Concurrency control: lightweight locks (latches)

*   Optimization
    * copy-on-write in place of WAL

### B-Trees vs. LSM-Trees

#### LSM-Trees Advantages

*   Faster writes in LSM-trees; fast reads in B-tree
*   Higher write throughput in LSM-trees.
    *   sequential writes in LSM-tree; random writes in B-tree.
    *   Caveat: write amplification (rewriting data multiple times over lifetime)
*   Smaller footprint in LSM-trees (better compression); fragmentation in B-tree.
*   SSD friendly.

#### LSM-Trees Downsides

*   Compaction interferes with read/write requests.
    *   when write throughput is very high, compaction could not keep up.
*   B-tree offers strong transactional semantics.

### Other indexing structures

*   Secondary indexes.
*   Storing values within the index
*   Multi-column indexes.
    *   R-trees for geospatial data.
*   Full-text search
    *   Search for *similar* keys based on edit distance.
    *   in-memory index is a finite state automaton.
*   In-memory databases: memcached
*   NVM (Non-volatile memory)

## Transaction Processing or Analytics

*   OLTP vs. OLAP.

### Data Warehousing

Data warehouse is OLAP database, which is isolated from business critical OLTP systems.

*   Vendors: Teradata, Vertica, ParAccel, Amazon RedShift, Apache Hive, Spark SQL, Cloudera Impala, Facebook Presto, Apaache Tajo, Apache Drill, and Google Dremel.

*   Fact tables vs. dimension tables.
*   A big enterprise may have tens of petabytes of transaction history.

## Column-Oriented Storage for Big Data Analytics

*   Store all the values from each column together.
*   Good when:
    *   Trillion rows; petabytes of data; and fact tables with +100 columns.
    *   A query only accesses 4 or 5 columns.

### Column Compression

*   To reduce the demands on disk throughput.
*   Methods
    *   bitmap encoding
    *   run-length encoding
        *   sort order in column storage so that same value is repeated many times
        *   replicating data with different sort orders

*   column-oriented storage vs. column families
*   memory bandwidth and vectorized processing

### Writing to Column-Oriented Storage

*   LSM-trees
    *   First go to an in-memory store
    *   Merged with the column files on disk.

### Aggregation: Materialized Views (and Data Cubes)

*   Materialized views: actual denormalized copy of the query results.
    *   Good for read-heavy data warehouses