# Designing Data-Intensive Application Ch 1

---

## Why build data-intensive application

* Blurry line
* No one-size-fits-all

---

## Reliability

* The system should continue to work _correctly_ (performing the correct function at the desired level of performance) even in the face of _adversity_ (hardware or software faults, and even human error).
* Continuing to work correctly, even when things go wrong.
* Build fault-tolerant system.
* Chaos Monkey

---

## Faults - Hardware faults

* Hardware
  * Traditionally: Redundancy of hardware components
  * Now in cloud: over-commit and other faults
  * Use software redundancy
    * Also get a _rolling upgrade_
  * [KIP-36 Rack aware replica assignment](https://cwiki.apache.org/confluence/display/KAFKA/KIP-36+Rack+aware+replica+assignment)
  * [GCE Live Migration](https://cloud.google.com/compute/docs/instances/live-migration)

---

## Software faults

* Software
  * More likely to be correlated
  * carefully thinking about assumptions and interactions in the system; 
  * thorough testing; 
  * process isolation; 
  * allowing processes to crash and restart; 
  * measuring, monitoring, and analyzing system behavior in production.

---

## Human errors

* Design APIs to minimize opportunities for error
* Non-prod sandbox
* Quick and easy recovery
  * Easy to roll back config
  * Gradual roll out
  * Tools to recompute data
* Monitoring

---

* [Summary of the Amazon S3 Service Disruption in the Northern Virginia (US-EAST-1) Region](https://aws.amazon.com/message/41926/)
  * command was entered incorrectly and a larger set of servers was removed.
    * necessary to serve all GET, LIST, PUT, and DELETE requests.
  * we have not completely restarted the index subsystem ... for many years.
  * We have modified this tool to remove capacity more slowly
  * auditing our other operational tools to ensure
  * breaking services into small partitions

---

## Scalability

* As the system grows (in data volume, traffic volume, or complexity), there should be reasonable ways of dealing with that growth.
* Load parameters: qps, read/write ratio, active users, cache hit rate...
* Twitter example: distribution of followers per user

---

## Describing Performance

* Throughput vs Response Time
* distribution of response time, percentiles: p90, p95, p99, p999
* how many 9s? Amazon: 3 9s
* SLO/SLA
* _head-of-line blocking_: Measure from client side

---

## Approaches for Coping with Load

* scale out vs scale up

---

## Maintainability

* Over time, many different people will work on the system (engineering and operations, both maintaining current behavior and adapting the system to new use cases), and they should all be able to work on it _productively_.
* Operability
* Simplicity: remove accidental complexity. use good abstraction
* Evolvability: agility on a data system level

