### JSON, XML, Protocol Buffers, Thrift and Avro
* How they handle schema changes
* How they support systems where old and new data and code need to coexist
  * Backward compatibility : newer code can read data that was written by older code
  * Forward compatibility : older code can read data that was written by newer code
* How those formats are used for data storage and for communication : in web services, REST and RPC, as well as message-passing systems

#### Formats for Encoding data
* Programs work with data in two different representations :
  * In memory - objects, structs, list, arrays, hash tables, trees and so on 
  * Sequence of bytes
* Definition of encoding - the translation from the in-memory representation to a byte sequence (also known as serialization or marshaling), the reverse is called decoding (parsing, deserialization, unmarshalling)

### Language-Specific formats
* built-in support : Java - java.io.Serializable, Ruby - Marshal, Python - pickle
* 3rd party library : Java - Kryo
* Pros : convenient, minimal additional code required
* Cons 
  * Tied to a particular programming language, reading the data in another language is very difficult.
  * In order to restore data in the same object types, the decoding process needs to be able to instantiate arbitrary classes.
    * Versioning data
    * Efficiency
    
### JSON,XML and Binary Variants
* XML : too verbose, unnecessarily complicated
* JSON : built-in support in web browser
* CSV : popular language-independent format, human-readable format
* Problems
  * ambiguity around the encoding of numbers
    * JSON : distinguishes string and numbers, but integers and floating-point numbers, doesn’t specify a precision
      * e.g. twitter 0 twitter’s API includes tweet IDs twice, once as a number and once as a decimal string
    * JSON and XML : support for Unicode character strings, but not for binary strings (sequences of bytes without a character encoding).
      * Encoding binary data as text using base64 : increase the data size by 33 %
    * Optional schema support for both XML and JSON
      * Schema languages : powerful, but complicated to learn and implement
       
#### Binary encoding
* JSON less verbose than XML, but still use a lot of space compared to binary formats
* Binary encodings for JSON - MessagePack, BSON, BJSON, UBJSON, BISON and Smil, for XML - WBXML and Fast Infest
  * Extend the set of datatypes 
  * Need all the object field names within the encoded data
    * Small space reduction and less human readability

### Thrift and Protocol Buffers
* Require a schema for any data
* Thrift
  * describe the schema in the Thrift interface definition language (IDL)
    * BinaryProtocol
    * CompactProtocol
    * Schema : mark either required (enables a runtime check) or optional.

##### Field tags and schema evolution
How do Thrift and Protocol Buffers handle schema changes while keeping backward and forward compatibility?
* Schema evolution : schemas inevitably need to change over time
  * Encoded record is the concatenation of encoded fields
  * Each field is identified by its tag number, so the name of a field can be changed, but not field’s tag

### Avro
* Schema to specify the structure of the data has two languages :
    - Avro IDL intended for human editing
    - Machine-readable
* No tags numbers in the schema
* Need exact same schema as the code that wrote the data to parse the binary data 

###### How does Avro support schema evolution?
* Writer’s schema and Reader’s schema
  * Need to be compatible
* Avro library resolves the differences of Writer's schema and Reader's schema
 
Writer's schema | Reader's schema | Action
---------|---------|---------
o | x | Ignore
x | o | Filled in with a default value declared in the reader's schema 

##### Schema evolution rules
* Add or remove a field that has a default value to maintain compatibility

##### But what is the writer's schema?
* Large file with lots of records
  * Include the writer's schema once at the beginning of the file  
* Database with individually written records
  * Include a version number at the beginning of every encoded record
  * Keep a list of schema versions in database
* Sending records over a network connection
  * netotiate the schema version on connection setup 
* Version number : incrementing integer or hash of the schema

##### Dynamically generated schemas
* Schema doesn't contain any tag number, so friendlier to dynamically generated schemas

##### Code generation and dynamically typed languages
* Thrift and Protocol Buffers 
  * Rely on code generation after a schema has been defined
  * Allows type checking and autocompletion in IDEs

#### The Merits of Schemas
* Simple and compact
* Valuable form of documentation
* Keeping a database of schmas allows you to check forward and backward compatibility
* For statically typed programming languages, enables type checking at compile time


### Modes of Dataflow
* Common ways how data flows between processes:
  * Via databases
  * Via service calls
  * Via asynchronous message passing

#### Dataflow Through Databases
* A single process encodes and decodes
  * In case, the reader can be a later version of the same process

##### Different values written at different times
* Replacing the old version with the new version of application takes within a few minutes
* Rewriting (migrating) data into a new schema
  * Expensive, avoid if possible
* Adding a new column with null default value without rewriting existing data


##### Archival storage
* When takeing a snapshot of database for backup or loading into a data warehouse, the data dump will typically be encoded using the latest schema

#### Dataflow Through Services: REST and RPC
* The most common arrangement is to have two roles : clients and servers
  * Clients make requests to web servers
    * Get : download HTML, CSS, JavaScript, images
    * Post : submit data to the server
  * Web browser, Native app running on a mobile device, desktop computer making network requests and Javascript application using AJAX, Server (e.g. app server)
  *  SOA or microservices architecture : decompose a large application into smaller services by area of functionality
  
##### Web services
* When HTTP is used as the underlying protocol for talking to the service
* Two popular approaches to web services : REST and SOAP
  * REST : not a protocol, but rather a design philosophy
    * Often associated microservices
    * RESTful : API designed according to the principles of REST
  * SOAP : XML-based protocol for making network API requests
    * API : XML-based languages called the Web Services Description Language, or WSDL
    * WSDL : too complex to construct manually, so heavily rely on tool support, code generation and IDEs

##### The problems with remote procedure calls(RPCs)
* Network request is unpredictable
* Network request may return without a result due to a timeout
* Network request is much slower than a function call
* Parameters need to be encoded into a sequence of bytes that can be sent over the network
* RPC framework must translate datatypes from one language into another	

##### Current directions for RPC
* Various RPC frameworks have been built on top of all the encodings
  * gRPC
    * RPC implementation using Protocol Buffers
    * Supports streams (a call consists of a series of requests and responses)
  * Finagle using Thrift
  * Rest.li using JSON over HTTP
* Custom RPC protocols with a binary encoding format can achieve better performance

##### Data encoding and evolution for RPC
* Servers will be updated first, then all the clients second
* Backward compatibility on requests and forward compatibility on response
* RPC schema 
  * Inherited from whatever encoding it uses
  * Thrift, gRPC and AVRO rpc : respective encoding format
  * SOAP : specified XML schemas
  * RESTful API : commonly use JSON
* Service compatibility is made harder by the fact that RPC is often used for communication across organizational boundaries











 