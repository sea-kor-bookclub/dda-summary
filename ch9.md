# Designing Data-Intensive Application Ch 9: Consistency and Consensus

Getting all the nodes to agree on something.

## Consistency Guarantees

* Most replicated DBs provide at least eventual consistency. However, this is a weak guarantee and required the application developer to consider the limitation.
* Providing a stronger consistency model since this is a common problem.
* There is some similarity btwn distributed consistency models and txn isolation models; however they are mostly independent as:
** txn isolation: avoiding race conditions
** distributed consistency: coordinating the state of replicas in the face of delays and faults

## Linearizability

Illusion of a single replica.
Has a lot of synonyms:

* atomic consistency
* strong consistency
* immediate consistency
* external consistency

As soon as one client successfully completes a write, all clients must read the value that was written.

### What Makes a System Linearizable

Data cannot flip back and forth - after any one read returns the new value, all following reads on any client must also return the new value.

```text
Serializability != Linearizability

Serializability is an isolation property of transactions. (Doesn't care about getting different results from different nodes. E.g. snapshot isolation.)
Linearizability is a recency guarantee on reads and writes of a register. (Doesn't care about txn isolation.)
```

### Relying on Linearizability

When is linearizability useful?

#### Locking and Leader election

* Electing a leader for single-leader replication.
* Often implemented by using coordination services like:
  * Apache ZooKeeper
  * etcd

#### Constraints and uniqueness guarantees

* Examples:
  * Unique username
  * Bank account balance never goes negative
  * ...

#### Cross-channel timing dependencies

* Having two channels for a write operation can cause ordering problems. (e.g. image resizer w/ image uploader and resizer message queue)

### Implementing Linearizable Systems

Single copy data store makes this trivial. However, not fault-tolerant.

* Single-leader replication (potentially linearizable)
* Consensus algorithms (linearizable)
* Multi-leader replication (not linearizable)
* Leaderless replication (probably not libearizable)

#### Linearizability and Quorums

Strict quorum reads and writes can still cause race conditions that break linearizability. Read repair can provide linearizability. However, this still doesn't provide a linearizable compare-and-set b/c it requires a consensus algorithm.

### The Cost of Linearizability

* Multi-leader is tolerant against DC network interruption.
* In the single-leader case, the DC w/o leader will be blocked.

#### CAP Theorem

* If you require linearizability, you will have to deal with unavailability.
* If you need availability, you cannot have linearizability.

The definition is pretty narrow so only has little practical value for designing systems.

#### Linearizability and network delays

* RAM isn't linearizable.
* Anything that caches result.
* This is for performance.

## Ordering Guarantees

### Ordering and Causality

* Causality imposes an ordering on events.
* If a system obeys the ordering imposed by causality, we say it is *causally consistent*.

#### The causal order is not a total order

* Linearizability: we know the *total order* of operations.
* Causality: Two events are concurrent if neither happened before the other. Defines a *partial order*.

In a linearizable datastore, there are no concurrent operations.

#### Linearizability is stronger than causal consistency

* If you only need causal consistency, you may be able to get better performance + availability.
* Research is quite recent; not much is available in production yet.

#### Capturing casual dependencies

Not too much detail; basically each write sends the read version.

### Sequence Number Ordering

Keeping track of causality can become impractical. One alternative is using *sequence numbers* or *timestamps*. These can be from a *logical clock*. In the case of single-leader replication, this counter can provide total ordering.

#### Noncausal sequence number generators

If there is not a single leader, it is hard to generate timestamps that reflect causality.

#### Lamport timestamps

* Total ordering!
* Consists of *(counter, node ID)*.
* Every node and every client keeps track of the maximum counter value it has seen so far and includes that maximum on every request.
* Upon receiving a counter value greater than self, it immediately increases its own value.
* Not possible to tell whether two operations are concurrent or causally dependent.

#### Timestamp ordering is not sufficient

* For example, uniqueness constraint can't be made.
* This requires checking every other node to make sure no other node is concurrently recording the same value.
* Need to know when that order is finalized.

### Total Order Broadcast

* Reliable Delivery: No messages are lost. If a message is delivered to one node, it is delivered to all nodes.
* Totally ordered delivery: Messages are delivered to every node in the same order.

#### Usage

* Database replication
* Serializable txns
* Acts like a log append
* Lock service that provides fencing tokens

#### Implementing linearizable storage using total order broadcast

1. Append a message to the log, indicating the username you want to claim.
1. Read the log, and wait for the message you appended to be delivered back to you.
1. Check for any messages claiming the username that you want. If the first message that comes back is yours, you can safely commit.

Above gives linearizable writes, but reads are not linearizable yet. For read, you can

* Append a message and then actually read after seeing the log message.
* Query the latest position and then wait for all entries up to that point before reading.
* Read from a replica that is synchronously updated on writes.

#### Implementing total order broadcast using linearizable storage

* For every message you want to send, you increment-and-get the linearizable integer and then attach the value as a sequence number to the message.
* Timestamp has no gaps so no guessing involved.

## Distributed Transactions and Consensus

Get several nodes to agree on something.

Usages:

* Leader election
* Atomic commit

### Atomic commit and Two-Phase Commit

#### Introduction

* Two phases of preparation and commitment
* If any of the participants do not reply yes to committing, the whole txn is aborted.

#### A system of promises

Two points of no return:

* When a participant votes yes, it can definitely commit later.
* When a coordinator decides, that decision is irrevocable.

#### Coordinator failure

This can lock up the txn if coordinator crashes after participant votes yes. Requires coordinator to recover.

#### Three-phase commit

Given a network w/ bounded delay and nodes w/ bounded response times can provide a nonblocking atomic commit. However, it breaks in a network w/ unbounded delay. Therefore, 2PC continues to be used.

### Distributed Txns in Practice

* On the one hand, provides an important safety guarantee.
* On the other, causes a lot of operational burden and slow performance.
* Two types:
  * Database-internal distributed transactions
  * Heterogeneous distributed transactions

#### Exactly-once message processing

Using heterogeneous distributed txns, you can implement a exactly once message queue.

#### XA(*eXtended Architecture*) transactions

* C API for interfacing w/ a txn coordinator.
* Other languages have this as well.
* Has the same caveat - coordinator crash requires manual intervention.

#### Holding locks while in doubt

* Probably at least a row level lock.
* In case of serializable isolation, the lock can be much wider.

#### Recovering from coordinator failure

* Mostly requires human intervention.
* Can use heristic decisions... but this is merely a euphemism for *probably breaking atomicity*

#### Limitations of distributed txns

* Single coordinator == single point of failure
* XA can't solve deadlock detection or provide SSI(Serializable Snapshot Isolation)

### Fault-Tolerant Consensus

Must satisfy:

* Uniform agreement: no two nodes decide differently.
* Integrity: no node decides twice.
* Validity: if a node decides value *v*, then *v* was proposed by some node.
* Termination: every node that does not crash eventually decides some value.

Termination => need fault tolerance.

#### Consensus algorithms and total order broadcast

Best known:

* Viewstamped Replication (VSR)
* Paxos
* Raft
* Zab

Most of these implement total order broadcast. Messages are delivered exactly once, in the same order, to all nodes.
Same as performing several rounds of consensus.

#### Single-leader replication and consensus

Need consensus to elect a leader; but to elect a leader we need total order broadcast.
If we need a leader for total order broadcast, there is a problem!

#### Epoch numbering and quorums

1. If current leader is dead
1. Vote is started w/ incremental epoch number
1. Larger number wins in conflict
1. Before leader decides anything, it checks that there isn't some other leader w/ a higher epoch number.
1. The node must collect votes from a quorum of nodes.
1. Repeated for every decision.

#### Limitations of consensus

* Requires synchronous replication => not as performant.
* Requires strict majority. In case of network fault, minority nodes may be blocked.
* Need fixed set of nodes. Dynamic membership extensions exist but are much less well understood.
* High variable network delays can cause a lot of leader elections.

### Membership and Coordination Services

ZooKeeper is mostly used indirectly.
Provides features like:

* Linearizable atomic operations
* Total ordering of operations
* Failure detection
* Change notifications

(See pg.370)

#### Allocating work to nodes

* Leader election
* Assigning partitions to nodes

Don't implement these by yourself - the success rate is pretty low.
Store slow changing data.

#### Service discovery

Can be used for service discovery. However, DNS for example uses multiple layers of cache for performance and availability. Stale records are usually not considered problematic.

#### Membership services

