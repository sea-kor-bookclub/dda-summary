# Chapter 8. The Trouble with Distributed System
Problems with network
Clocks and timing issues

## Faults and Partial Failures
Hardware problem (e.g.memory corruption or a loose connector) results in a total system failure (e.g., kernel panic, "blue screen of death").
Partial failure - some parts of the system are broken;while others are working fine, non-deterministic

#### Cloud Computing and Supercomputing
To build large-scale computing systems
- High-Performance Computing (HPC) - designed for intensive scientific computing tasks (e.g., weather forcasting or molecular dynamics)
- Cloud Computing - Associated with multi-tenant datacenters, commodity computers connected with an IP network (often Ethernet), elastic/on-demand resource allocation and metered billing

Comparison
- For partial failure, supercomputer acts like a single-node computer.
- Online job - stopping the cluster for repair unacceptable; Offline (batch) jobs can be stopped and restarted
- Supercomputers - built from specialized hardware; Cloud services - built from commodity machines, low cost, higher failure rates.
- Datacenter networks - based on IP and Ethernet, Clostopologies for high bisection bandwidth; Supercomputer - specialized network topologies for better performace.
- Bigger system - more issues.
- Tolerate failed nodes and keep working as a whole is useful for operation and maintenance.
- Geographically distributed deployment - slow and unreliable; Supercomputer - nodes are close together.

## Unreliable Networks
Distributed system 
- Network is the way to ommunicate between machines; shared-nothing system.
- Asynchronous packet networks (fig.8-1)
    * Request lost
    * Request waiting in a queue
    * Remote node failed
    * Remote node on hold (e.g., GC)
    * Response lost
    * Response delayed
- Timeout

#### Network Faults in Practice
- Human errors : e.g.) misconfigured switches (major cause of outages)
- Frequent transient network glitches
- Damanged undersea cables by sharks 
- Working well in one direction (e.g., dropping all inbound packets)
- Cluster deadlocked

Trigger network problems and test the system's response : e.g., ChaosMonkey

#### Detecting Faults
Many systems need to automatically detect faulty nodes : e.g., Load balander, single-leader replication
- Node process crashed;but operation system is still running : Script can be used to notify other nodes
- Management interface of the network switches : Detect link failure e.g., power down
- Router : Sure that destination IP address is unreachable e.g., ICMP Destination Unreachable packet
- Retry and wait for a timeout to elapse

#### Timeouts and Unbounded Delays
Long timeout - Long wait util a node is declared dead
Short timeout - Detects faults faster, carries a higher risk of incrrectly declaring a node dead
    * e.g., cascading failure : slow to respond due to overload; transferring its load to other nodes
Fictitious system - mamximum delay is 2d + r (d : delivery time, r : handling a request time)
most systems : asynchronous networks, unbounded delays (deliver packets as quickly as possible)

##### Network congestion and queueing
Variability of packet delays on computer networks is most often due to queueing
- Different node simulatenously try to send packets to the same detination
- In all CPU cores are busy, incoming request from the network is queued
- In Virtualized environment, a running operation is often paused for tens of milliseconds while another virtual machine uses a CPU core
- TCP performs flow control; limits its own rate of sending

#### Synchronous Versus Asynchronous Networks
Synchronous : even as data passes through several routers, it does not suffer from queueing
Bounded delay : maximum end-to-end latency of the network is fixed

## Unreliable Clocks
- Durations
    - Has this request timed out yet?
    - What's the 99th percentile response time of this service?
    - How many queries per second idd this service handle on average in the last five minutes?
    - How long did the user spend on our site?
- points in time
    - When was this article published
    - At what date and time should the reminder email be sent?
    - When does this cache entry expire?
    - What is the timestamp on this error message in the log file?

- Each machine on the network has its own clock; hardware device - usually a quartz crystal oscillator
- NTP(Network Time Protocol) : allows the computer clock to be adjusted according to the time reported by a group of servers

#### Monotonic Versus Time-of-Day Clocks
##### Time-of-day Clocks
- Current date and time according to some calendar (a.k.a wall-clock time)
- In Java, the number of seconds (milliseconds) since January 1, 1970 (UTC)
- Synchronized with NPT
- Unsuitable for measuting elapsed time e.g., go back and forward

##### Monotonic clokcs
- Suitable for measuing a duration : timeout or a service's response time
- Measure difference between two values

#### Clock Synchronization and Accuracy
- Quartz clock in a computer is not very accurate e.g., temperature
- If computer's clock differes too much from an NTP server, refuse to synchronize or local clock will be reset
- NPT synchronization can only be as good as the network delay
- Some NTP servers are wrong or misconfigured

GPS receivers, the Precision Time Protocol (PTP) : high-frequency trading funds to synchronize their clocks to within 100 microseconds

#### Relying on Synchronized Clocks
##### Timestamps for ordering events
- Dangerous use of time-of-day clocks in a database with multileader replication (fig. 8-3)
- Conflict resolution strategy : LWW (last write wins)
    * Database writes can mysteriously disappear
    * LWW cannot distinguish between writes that occurred sequentially in quick succession, and writes that were truly concurrent
    * It's possible for two nodes to independently generate writes with the same timestamp

Logical clocks - incrementing counters rather than an oscillating quartz crystal, measure only the relative ordering of events

##### Clock readings have a confidence interval
Google's TrueTime API in Spanner : [earliest, latest]

##### Synchronized clocks for global snapshots
Transaction Id - from synchronized time-of-day clocks
Spanner : e.g., A (earliet) < A (latest) < B (earliest) < B (latest) : B happened after A

#### Process Pauses
- Lease for lock with a timeout
- GC (Garbage collector)
- VM can be suspended and resumed
- End-user devices execution may be suspended and resumed arbitrarily
- Operating system context-switches
- Performs synchronous disk access : waiting for a slow disk I/O operation to complete
- Swapping to disk (paging)
- Unix process : paused by sending it the SIGSTOP signal

##### Response time guarantees
- Specified deadline by which the software must respond (e.g., aircraft, rockets, robots, cars)
- RTOS (real-time operating system) : allows processes to be scheduled with a guaranteed allocation of CPU time in specified intervals is needed; worst-case execution times; dynamic memory allocation may be restricted or disallowed.

##### Limiting the impact of garbage collection
- Treat GC pauses like brief planned outages of a node, and to let other nodes handle requests from clients whilte one node is colleting its garbage.

## Knowledge, Truth, and Lies
#### The Truth Is Defined by the Majority
- Distributed system : rely on a quorum; voting among the nodes

##### The leader and the lock
- Only one node is allowed to be the leader for a database partition
- Only one transaction or client is allowed to hold the lock for a particular resource or object
- Only one user is allowed to register a particular username
e.g. data corruption bug due to an incorrect implementation of locking (fig 8-4) 

##### Fencing tokens
Fencing token : a number that increases every time a lock is granted (fig 8-5) e.g., ZooKeeper : transaction ID zxid or the node version cversion can be used as fencing token

#### Byzantine Faults
Byzantine fault : if a node may claim to have received a particular message when in fact it didn't
Byzantine Generals Problem : the problem of reaching consensus in this untrusting environment
- Aerospace environment e.g., CPU register corrupted by radiation
- Multiple participating organizations : some participants may attempt to cheat or defraud others e.g., peer-to-peer networks like Bitcoin
- Web applications e.g., malicious behavior of clients

#### Weak forms of lying
- Mechanisms to software that guard against weak formet of "lying"
    * Checksums built into TCP and UDP for corrupted packets
    * Sanitize any inputs from users for a publicly accessible application
    * Configure NTP clients with multiple server addresses

#### System Model and Reality
- Synchronous model : assumes bounded network delay, bounded process pauses, and bounded clock error
- Partially synchronous model : behaves like a synchronous system most of the time, but it sometimes exceeds the bounds for network delay, process pauses, and clock drift
- Asynchronous model : algorithm is not allowed to make any timing assumptions
- Crash-stop faults : if node may suddenly stop responding at any moment, and thereafter that node is gone forever
- Crash-recovery faults : nodes may crash at any moment, and perhaps start responding again after some unknown time
- Byzantine faults : Nodes may do absolutely anthing, including trying to trick and deceive other nodes

##### Correctness of an algorithm
- Uniqueness : No two request for a fencing token return the same value
- Monotonic sequence 
- Availability : A node that requests a fencing token and does not crash eventually receives a response.