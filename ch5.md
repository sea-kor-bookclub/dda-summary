# Part II: Distributed Data
* Why distribute to multiple machines
  * Scalability (throughput), Fault tolerance (availability), Latency

* Scaling to higher load
  * scale up (or vertical scaling)
    * shared-memory architecture
    * shared-disk architecture
  * scale out, horizontal scaling, shared-nothing architecture
    * (+) use commodity hardwares
    * (-) complexity

* Replication versus partitioning
  * replication: multiple nodes with the same data
  * partitioning: multiple nodes with smaller subsets

# Replication

* main challenge: handling changes to replicated data
  * approaches: single-leader, multi-leader, leaderless

## Leaders and Followers

* leader-based replication
  * active/passive or master/slave
  * leader
    * master, primary
    * handle write requests
    * send changes to followers in replication log or change stream
  * followers
    * read replicas, slaves, secondaries, hot standby
    * process writes in the same order

* synchronous versus asynchronous replication
  * wait for a response from follower or not
  * synchronous
    * (+) up-to-date copy in follower
    * (-) halt if follower fails
    * semi-synchronous: enable synchronous for one follower
  * asynchronous
    * (+) availability
    * (-) durability
    * variant: chain replication

* setting up new followers
  * without downtime
  * take a snapshot of leader
  * copy snapshot to follower
  * follower processes the back of data changes since the snapshot
    * log sequence number, binlog coordinates
  * follower caught up

* Handling note outages
  * follower failure: catch-up recovery
    * follower requests data changes from leader, and apply
  * leader failure: failover
    * elect a new leader, reconfigure clients
    * determine that the leader has failed: using timeout
    * choosing a new leader: consensus protocol
    * reconfigure system to use the new leader
      * should ensure the former leader becomes a follower
    * problems
      * possible data loss
      * inconsistent with external system
      * split brain
      * tune timeout

* Implementation of replication logs
  * statement-based replication
    * sends write requests to followers (ex, SQL)
    * problems:
      * nondeterministic function: NOW(), RAND()
      * auto-incrementing column or dependency on existing data
      * side effects: triggers, stored procedures
  * write-ahead log (WAL) shipping
    * sends logs to followers: append-only sequence of bytes
    * problem:
      * very low-level, closely coupled to the storage engine
      * not allow different versions
  * logical (row-based) replication
    * higher-level log format for replication
    * relational db: granularity of a row
    * decoupled from storage internals, allowing multiple versions
  * trigger-based replication
    * use triggers and stored procedures
    * handled in application layer
    * more flexibility with bigger overheads

## Problems with replication lag

* read-scaling architecture
  * add more followers to increase capacity for read requests
  * need asynchronous replication
  * replication lag: eventual consistency (no guarantee on the lag)
* reading your own writes
  * read-after-write consistency, or read-your-writes consistency
  * how?
  * read from leader: ex) user's own profile
  * monitor last update time, or replication lag
  * client remembers last update, compares with replica's last update
* cross-device read-after-write consistency
  * hard to remember last update
  * may connect to different datacenters
* monotonic reads
  * users will not see time go backward
  * stronger guarantee than eventual consistency
  * always read from the same replica
* consistent prefix reads
  * client will see writes in the same order
  * problem in partitioned databases
  * have casually-related writes in the same partition
* Solutions for replication lag: transaction?

## Multi-leader replication
* multi-leader
  * master-master replication, active/active replication
  * more than one node accept writes
  * each leader acts as a follower to the other leaders
  * multi-datacenter operation
* handling write conflicts
  * conflict is detected asynchronously
  * conflict avoidance: same leader for a particular record
  * converge toward a consistent state
    * last write wins (LWW): (-) data loss
    * replica with a higher ID: (-) data loss
    * somehow merge
    * record the conflict and resolve later
  * custom conflict resolution logic:
    * on write: callback
    * on read: return multiple versions, and let the client resolve
* multi-leader replication topologies
  * circular, star, tree, all-to-all
  * circular, all-to-all: one node failure can interrupt replication flow
  * all-to-all: messages delivered in different orders


## Leaderless replication
* old idea became a fashionable architecture
  * Dynamo-style: Riak, Cassandra, Voldmort
  * directly send writes to replicas vs. use coordinator
* writing to the database when a node is down
  * 3 nodes
  * client sends writes to all nodes, proceeds when 2 acks
  * client sends reads to all nodes, finds the up-to-date value
* read repair and anti-entropy
  * read repair: client fixes when stale data is detected
  * anti-entropy process: background process for finding and copying missed data
* quorums for reading and writing
  * n replicas, n for writes, r for reads
  * quorum condition: w + r > n
  * ex) read-heavy, set r low
* limitations of quorum consistency
  * sloppy quorum: no guarantee for up-to-date value
  * possible data loss for concurrent writes
  * non-deterministic reads for concurrent read and write
  * partially successful writes, will still return the value
  * replicas can fall below w later
  * no guarantee: read-after-write, monotonic reads, consistent prefix reads
* monitoring staleness
  * leader-based application: difference in positions
  * leaderless: harder, but important?
* sloppy quorums and hinted hand-off
  * temporarily use nodes that are outside of n designated nodes
  * in case that quorum condition can't be met
  * hinted hand-off: copy from temporary nodes to recovered nodes
* detecting concurrent writes
  * events may arrive in a different order
  * last write wins (LWW)
    * overwrite older value with recent value
    * possible data loss in concurrent writes
    * option: a key is written once and become immutable
  * happens-before relationship and concurrency
    * A's insert happens before B's increment
    * B is casually dependent on A
    * B knows about A, or depends on A, or builds upon A
  * capturing the happens-before relationship
    * writes request: version number from previous write, value
    * response: (version, value) pairs
    * assuming 1 replica, see Figure 5-13
    * (-) requires requires the clients to handle merge
    * cart example: union with tombstone for removed items
  * version vectors
    * multiple replicas
    * version number per replica and per key
